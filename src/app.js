const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const ansible = require("node-ansible");

app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

app.use(express.json());

// respond with "hello world" when a GET request is made to the homepage
app.get("/", (req, res) => {
  res.send("<h1>Baywa REST API</h1>");
});

let autoIncrement = 3;

const customers = [
  {
    // 0
    id: 1,
    firstName: "Bernd",
    lastName: "Huber",
  },
  {
    // 1
    id: 2,
    firstName: "Tanja",
    lastName: "Müller",
  },
];

app.get("/customers", (req, res) => {
  res.json({items: customers});
});

app.get("/customers/:id", (req, res) => {
  console.log(req.params.id);

  if (!req.params.id) {
    return res.status(400).json({ message: "No id" });
  }

  const customer = customers.find(
    (customer) => customer.id === parseInt(req.params.id)
  );

  if (!customer) {
    return res.status(404).json({ message: "Customer not found" });
  }

  res.json({item: customer});
});

app.post("/customers", (req, res) => {
  console.log(req.body);

  if (!req.body) {
    return res.status(400).json({ message: "No body" });
  }

  if (!req.body.firstName) {
    return res.status(400).json({ message: "No first name" });
  }

  const newCustomer = {
    ...req.body,
    id: autoIncrement,
  };

  autoIncrement++;

  customers.push(newCustomer);

  res.json({ message: "Customer created", item: newCustomer });
});

app.delete("/customers/:id", (req, res) => {
  console.log(req.params.id);

  if (!req.params.id) {
    return res.status(400).json({ message: "No id" });
  }

  const index = customers.findIndex(
    (customer) => customer.id === parseInt(req.params.id)
  );

  if (index === -1) {
    return res.status(404).json({ message: "Customer not found" });
  }

  customers.splice(index, 1);

  res.json({ message: "Customer deleted", items: customers });
});

app.put("/customers", (req, res) => {
  console.log(req.body);

  if (!req.body) {
    return res.status(400).json({ message: "No body" });
  }

  if (!req.body.firstName) {
    return res.status(400).json({ message: "No first name" });
  }

  const index = customers.findIndex(
    (customer) => customer.id === parseInt(req.body.id)
  );

  if (index === -1) {
    return res.status(404).json({ message: "Customer not found" });
  }

  customers[index] = req.body;

  res.json({ message: "Customer updated", items: customers });
});

app.post("/ansible", async (req, res) => {
  const command = req.body.command;

  if (!command) {
    return res.status(400).json({ message: "No command" });
  }

  const ansibleCommand = new ansible.AdHoc()
    .module("shell")
    .hosts("local")
    .user('root')
    .args(command);

  const result = await ansibleCommand.exec();

  res.json({ message: "Ansible executed", result });
});

app.listen(3000, () => {
  console.log(`Example app listening on port`);
});
